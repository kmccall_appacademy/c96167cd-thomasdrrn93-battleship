class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    idx1 = pos[0]
    idx2 = pos[1]
    board.grid[idx1][idx2] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    turn = player.get_play
    self.attack(turn)
  end
end
