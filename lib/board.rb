class Board

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  attr_accessor :grid

  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def count
    counter = 0

    @grid.each do |row|
      row.each do |col|
        if col != nil
          counter += 1
        end
      end
    end

    counter
  end

  def empty?(pos = nil)
    if pos
      idx1 = pos[0]
      idx2 = pos[1]
      self.grid[idx1][idx2] == nil
    else
      self.grid.flatten.all? {|el| el == nil}
    end
  end

  def full?
    self.grid.flatten.none? {|slot| slot == nil}
  end

  def random_pos
    [rand(size), rand(size)]
  end

  def size
    grid.length
  end

  def place_random_ship
    raise "error" if self.full?
    pos = random_pos
    until empty?(pos)
      pos = random_pos
    end
    self[pos] = :s
  end

  def won?
    self.grid.flatten.none? {|el| el == :s}
  end
end
